// Bab 5 Nomor 5
// Tulislah program Pascal/C/C++ untuk menghitung gaji PNS yang
// terdiri dari gaji pokok, tunjangan tunjangan anak, dan tunjangan istri

#include <iostream>
using namespace std;

main() {
    cout << "===>> Bab 5 Nomor 5 <<===\n";
    // Program Menghitung gaji PNS
    cout << "<========== Program Menghitung Gaji PNS ==========>\n";
    unsigned long int gaji_pokok, tunjangan_istri, anak, tunjangan_anak, total_gaji;
    cout << "Masukkan gaji pokok anda : ";
    cin >> gaji_pokok;

    tunjangan_istri = 0.1*gaji_pokok;
    
    cout << "Berapa anak yang anda punya (Diberikan paling banyak 3 orang anak) ? ";
    cin >> anak;
    if (anak >= 3) {
        tunjangan_anak = 0.02*gaji_pokok*3;
    }
    else {
        tunjangan_anak = 0.02*gaji_pokok*anak;
    }
    
    total_gaji = gaji_pokok + tunjangan_istri + tunjangan_anak;
    cout << "Gaji Pokok      = Rp" << gaji_pokok << endl;
    cout << "Tunjangan Istri = Rp" << tunjangan_istri << endl;
    cout << "Tunjangan " << anak << " anak = Rp" << tunjangan_anak << endl;
    cout << "Total gaji yang diperoleh = Rp" << total_gaji << endl; 
}
