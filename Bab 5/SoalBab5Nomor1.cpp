// Bab 5 Nomor 1
// Translasilah algoritma menghitung luas persegi panjang dan algoritma sapaan
// di dalam bab 4 ke dalam program Pascal, C, atau C++

#include <iostream>
using namespace std;

main() {
    cout << "===>> Bab 5 Nomor 1 <<===\n";
    // Program Menghitung Luas
    cout << "<========== Program Menghitung Luas Persegi Panjang ==========>\n";
    float p, l, luas;
    cout << "Masukkan panjang (cm) : ";
    cin >> p;
    cout << "Masukkan lebar (cm) : ";
    cin >> l;

    luas = p*l;
    cout << "Panjang : " << p << " cm\n";
    cout << "Lebar   : " << l << " cm\n";
    cout << "~~Hasil perhitungan luas persegi panjang~~ \n";
    cout << "==>> Luas persegi panjang : Panjang x Lebar\n";
    cout << "==>> Luas persegi panjang : " << p << " x " << l << endl;
    cout << "==>> Luas persegi panjang : " << luas << " cm^2 \n";

    cout << "\n";
    // Program Sapaan
    cout << "<========== Program Sapaan ==========>\n";
    string nama, kota;
    cout << "~> Halo, siapa namamu ? ";
    cin >> nama;
    cout << "~> Dikota apa kamu sekarang ? ";
    cin >> kota;
    cout << "Senang berteman denganmu, " << nama << ", di kota " << kota << " ~(^_^)~" << endl;
}
