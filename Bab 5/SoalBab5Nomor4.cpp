// Bab 5 Nomor 4
// Tulislah program Pascal/C/C++ yang menghitung harga yang dibayar pembeli 
// ketika makan di restoran. Pajak makanan di restoran adalah p%

#include <iostream>
using namespace std;

main() {
    cout << "===>> Bab 5 Nomor 4 <<===\n";
    // Program Pajak Restoran
    cout << "<========== Program Menghitung Pajak Makan di Restoran ==========>\n";
    unsigned int pilih;
    string ya;
    float harga = 0;
    float menu_1 = 15000, menu_2 = 17000, menu_3 = 12000, menu_4 = 10000, menu_5 = 7000, menu_6 = 8000, menu_7 = 5000, menu_8 = 6000, menu_9 = 6000, menu_10 = 13000; 

    do {
    cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
    cout << "||              MAKANAN              ||\n";
    cout << "|| 1.  Nasi Goreng          Rp15.000 ||\n";
    cout << "|| 2.  Kwetiau Spesial      Rp17.000 ||\n";
    cout << "|| 3.  Ayam Bakar           Rp12.000 ||\n";
    cout << "|| 4.  Sate Sapi            Rp10.000 ||\n";
    cout << "|| 5.  Roti Bakar           Rp7.000  ||\n";
    cout << "||===================================||\n";
    cout << "||              MINUMAN              ||\n";
    cout << "|| 6.  Lemon Tea Ice/Hot    Rp8.000  ||\n";
    cout << "|| 7.  Teh Manis            Rp5.000  ||\n";
    cout << "|| 8.  Jus Jeruk            Rp6.000  ||\n";
    cout << "|| 9.  Jus Alpukat          Rp6.000  ||\n";
    cout << "|| 10. Susu Ice/Hot         Rp13.000 ||\n";
    cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
    ulang:
    cout << "Pilih menu yang dipesan : ";
    cin >> pilih;

    if (pilih == 1) {
        int count_1, temp_1;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_1;
        temp_1 = menu_1*count_1;
        harga += temp_1;
        cout << "Anda memesan " << count_1 << " Nasi Goreng \n"; 
        cout << "==> Harga total : Rp" << harga << endl; 
    }
    else if (pilih == 2) {
        int count_2, temp_2;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_2;
        temp_2 = menu_2*count_2;
        harga += temp_2;
        cout << "Anda memesan " << count_2 << " Kwetiau Spesial \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else if (pilih == 3) {
        int count_3, temp_3;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_3;
        temp_3 = menu_3*count_3;
        harga += temp_3;
        cout << "Anda memesan " << count_3 << " Ayam Bakar \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else if (pilih == 4) {
        int count_4, temp_4;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_4;
        temp_4 = menu_4*count_4;
        harga += temp_4;
        cout << "Anda memesan " << count_4 << " Sate Sapi \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else if (pilih == 5) {
        int count_5, temp_5;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_5;
        temp_5 = menu_5*count_5;
        harga += temp_5;
        cout << "Anda memesan " << count_5 << " Roti Bakar \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else if (pilih == 6) {
        int count_6, temp_6;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_6;
        temp_6 = menu_6*count_6;
        harga += temp_6;
        cout << "Anda memesan " << count_6 << " Lemon Tea \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else if (pilih == 7) {
        int count_7, temp_7;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_7;
        temp_7 = menu_7*count_7;
        harga += temp_7;
        cout << "Anda memesan " << count_7 << " Teh Manis \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else if (pilih == 8) {
        int count_8, temp_8;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_8;
        temp_8 = menu_8*count_8;
        harga += temp_8;
        cout << "Anda memesan " << count_8 << " Jus Jeruk \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else if (pilih == 9) {
        int count_9, temp_9;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_9;
        temp_9 = menu_9*count_9;
        harga += temp_9;
        cout << "Anda memesan " << count_9 << " Jus Alpukat \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else if (pilih == 10) {
        int count_10, temp_10;
        cout << "Berapa porsi yang diinginkan ? ";
        cin >> count_10;
        temp_10 = menu_10*count_10;
        harga += temp_10;
        cout << "Anda memesan " << count_10 << " Kwetiau Spesial \n";  
        cout << "==> Harga total : Rp" << harga << endl;
    }
    else {
        cout << "Tidak ada dalam menu. Silahkan pilih ulang...\n";
        goto ulang;
    }
    cout << "Apakah akan memesan menu lainnya (y/t) ? ";
    cin >> ya;
    } while(ya == "y" || ya == "Y");
    
    float service_tax, dpp, pb1, total_harga;
    service_tax = 0.05 * harga;
    cout << "Pajak layanan (service charge) : 5% x harga \n";
    cout << "Pajak layanan (service charge) : 5% x " << harga << endl;
    cout << "Pajak layanan (service charge) : Rp" << service_tax << endl;

    cout << "\n";
    dpp = harga + service_tax;
    cout << "Dasar Pengenaan Pajak (DPP) : harga + service tax\n";
    cout << "Dasar Pengenaan Pajak (DPP) : " << harga << " + " << service_tax << endl;
    cout << "DPP : Rp" << dpp << endl;
    
    cout << "\n";
    cout << "Tarif pajak restoran = 10% \n";
    pb1 = dpp * (0.1);
    cout << "PB1 (Pajak Restoran) : DPP x 10%" << endl;
    cout << "PB1 (Pajak Restoran) : " << dpp << " x 10% \n"; 
    cout << "PB1 (Pajak Restoran) : Rp" << pb1 << endl;

    cout << "\n";
    total_harga = dpp + pb1;
    cout << "Total harga keseluruhan : DPP + PB1" << endl;
    cout << "Total harga keseluruhan : " << dpp << " + " << pb1 << endl;
    cout << "Total harga keseluruhan : Rp" << total_harga << endl;
}
