// Bab 5 Nomor 3
// Tulislah program Pascal/C/C++ untuk menghitung keliling lingkaran

#include <iostream>
using namespace std;

main() {
    cout << "===>> Bab 5 Nomor 3 <<===\n";
    // Program Menghitung Keliling Lingkaran
    cout << "<========== Program Menghitung Keliling Lingkaran ==========>\n";
    float keliling, r, d, phi = 3.14;
    int pilih;

    cout << "====================\n";
    cout << "|| 1. Jari - Jari ||\n";
    cout << "|| 2. Diameter    ||\n";
    cout << "====================\n";
    ulang:
    cout << "Pilih salah satu : ";
    cin >> pilih;

    if (pilih == 1) {
        cout << "Masukkan jari - jari lingkaran (cm) : ";
        cin >> r;
        keliling = 2*phi*r;
        cout << "Rumus Keliling Lingkaran : 2 x phi x r \n";
        cout << "Hasil hitungan keliling lingkaran : " << keliling << " cm\n";
    }
    else if (pilih == 2) {
        cout << "Masukkan diameter lingkaran (cm) : ";
        cin >> d;
        keliling = phi*d;
        cout << "Rumus Keliling Lingkaran : phi x d \n";
        cout << "Hasil hitungan keliling lingkaran : " << keliling << " cm\n";
    }
    else {
        cout << "Tidak ada dalam daftar silahkan ulangi \n";
        goto ulang;
    }
}
