#include <iostream>
using namespace std;

main()
{

	// deklarsi
	int N, i, j;
	
	// algoritma
	cout << " Tinggi segitiga N = "; cin >> N;
	
	// cetak bagian segitiga dari baris 1 sampai N 
	
	for (i=1; i<=N; i++)
	{
		for (j=1; j<=i; j++)
		{
			cout << "*";
		}
		cout << endl;
	}
}
